# Redenge

Balíček `redenge/redenge` obsahuje:
- **Redenge\Admin\ModuleLoader**, který slouží k načítání modulů do administrace (nutná úprava na eshopu, viz níže)
- **Redenge\Money\CNB\ExchangeRate**, který tahá kurzy měn z API ČNB vůči CZK

## Instalace

1) Závislosti nainstalujeme pomocí [Composer](https://getcomposer.org):

```bash
composer require redenge/redenge
```
> V composer.json projektu je potřeba mít `"repositories": [{"type": "composer", "url": "https://satis.redenge.biz"}]`

### ModuleLoader

Pro použití ModuleLoader je potřeba udělat pár úprav na shopu. Niže uvedený seznam úprav se můžeš lišit.

2) V `admin/index.php` inicializujeme `ModuleLoader` s volitelnými moduly:

```php
// admin/index.php
$adminModuleLoader = new Redenge\Admin\ModuleLoader($engine, $_components, [
	'Some\Fancy\Module', // Název třídy která implementuje Redenge\Admin\IModulePackage
]);
```

3) Upravíme `engine/admin.php`

```php
// Původní řádky nahradíme metodou ModuleLoader::addModuleLinkToTemplate()
// $tplpage->assign("NAME", $module["caption"]);
// $tplpage->assign("LINK", sprintf("%s?c=%s&amp;m=%s", $_base_link, $component_name, $module["name"]));
$adminModuleLoader->addModuleLinkToTemplate($tplpage, $module, $_base_link, $_component_name);

...

// Pod řádek s
$_actual_module_name = $module["caption"];
// Přidáme
$adminModuleLoader->setCurrentModule($module);

...

// A upravíme cesty z
// $include_file = sprintf("%s/component/%s/modul/%s/start.php", dirname(__FILE__), $_component_name, $_modul_name);
// $include_main_file = sprintf("%s/component/%s/include.php", dirname(__FILE__), $_component_name);
// Na
$include_file = sprintf("%s/component/%s/modul/%s/start.php", $adminModuleLoader->getCurrentModulePath(__DIR__), $_component_name, $_modul_name);
$include_main_file = sprintf("%s/component/%s/include.php", $adminModuleLoader->getCurrentModulePath(__DIR__), $_component_name);

```

4) `ShopModel.php` rozšíříme o metody (pokud již nejsou)

```php
/**
 * @param Model $model Model envelope
 */
public function addModel(Model $model)
{
	$this->models[$model->getName()] = $model;
}

/**
 * @return Core|Database
 */
private function getModelDependency()
{
	return $this->compat ? $this->core : $this->core->DB;
}
```

5) Přepíšeme metodu __get()

```php
/**
 * Magic models getter
 */
public function __get($_var) {
	if (!array_key_exists($_var, $this->models)) {
		throw new Exception("Model '$_var' is not defined in ShopModel.");
	}

	if ($this->models[$_var] === null) {
		$this->models[$_var] = new $_var($this->getModelDependency(), $this);
	} elseif ($this->models[$_var] instanceof Model) {
		$className = $this->models[$_var]->getClass();
		$this->models[$_var] = new $className($this->getModelDependency(), $this);
	}

	if (!($this->models[$_var] instanceof DBObject)) {
		throw new Exception("Object '$_var' is not instance of DBObject.");
	}

	return $this->models[$_var];
}
```

#### Todo
- Modelové třídy (potomci `\DBObject`) jsou takto dostupné v administraci, pro fronendové části je potřeba je přidat manuálně do ShopModel (např. `$this->models['eet'] = new \Redenge\Admin\Model('eet', 'Redenge\EET\Model\Transaction');`)
- Pro nové **Nette** moduly do administrace (např. JRC) je potřeba přidat tyto moduly do levého menu modulů

### ExchangeRate
```php
$rates = \Redenge\Money\CNB\ExchangeRateFactory::create();
try {
	$byService = TRUE;
	$euroRate = $rates->get(currency::EURO_CODE);
} catch (\Redenge\Money\CNB\Exception\Exception $e) {
	$byService = FALSE;
	$euroRate = $engine->shop->currency->getExchangeRateByCode(currency::EURO_CODE); // Alternativní získání kurzu, liší se per eshop
}

bdump([
	'Kurz CZK/EUR' => $euroRate,
	'by_service' => $byService,
]);
```

`ExchangeRateFactory` má první volitelný parametr `Nette\Caching\IStorage`, pokud se nepředá, pro cachování se použije `Nette\Caching\Storages\FileStorage`.

## Požadavky
- PHP: >= 5.6
- nette/utils: ~2.3
- nette/forms: ~2.3
- nette/caching: ~2.3
