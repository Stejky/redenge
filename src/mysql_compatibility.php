<?php

function mysqlc_fetch_assoc($var)
{
	return is_resource($var) ? mysql_fetch_assoc($var) : mysqli_fetch_assoc($var);
}

function mysqlc_free_result($var)
{
	return is_resource($var) ? mysql_free_result($var) : mysqli_free_result($var);
}

function mysqlc_num_rows($var)
{
	return is_resource($var) ? mysql_num_rows($var) : mysqli_num_rows($var);
}

// class MySQLc
// {
// 	const MYSQL = 0;
// 	const MYSQLI = 1;
// 	const DETECT = NULL;

// 	public static $mode = self::DETECT;

// 	public static function enable()
// 	{
// 		self::$mode = self::detect();
// 	}

// 	public static function detect()
// 	{
// 		return function_exists('mysqli_fetch_array');
// 	}
// }

// MySQLc::enable();
