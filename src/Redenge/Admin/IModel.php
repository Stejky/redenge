<?php

namespace Redenge\Admin;

interface IModel
{
	/**
	 * @return string
	 */
	public function getName();

	/**
	 * @return string
	 */
	public function getClass();
}
