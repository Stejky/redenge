<?php

namespace Redenge\Admin;

interface IModulePackage
{
	/**
	 * @return array
	 */
	public function getModels();

	/**
	 * @return array
	 */
	public function getModules();
}
