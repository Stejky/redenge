<?php

namespace Redenge\Admin;

/**
 * Loads admin modules and models if available
 */
class ModuleLoader extends \Nette\Object
{
	const REDENGE = 'redenge';
	const NETTE = 'nette';

	/** @var \Engine */
	private $engine;

	/** @var \Nette\Application\LinkGenerator */
	private $linkGenerator;

	/** @var array */
	private $modules = [];

	/** @var */
	private $currentModule;

	/**
	 * @todo Parameters $engine and $_components are passed as reference :(
	 * @todo Check if module or model doesn't exists
	 *
	 * @param \Engine    $engine
	 * @param array      $components
	 * @param array      $packages
	 */
	public function __construct(\cinterface &$engine, array &$components, array $packages)
	{
		$this->engine = $engine;

		foreach ($packages as $package) {
			// bdump('Loading..: ' . $module);

			// if (!class_exists($module)) {
			// 	throw new Exception(sprintf('Admin module "%s" does not exists.', $module));
			// }

			$package = new $package();

			/**
			 * Add models
			 */
			foreach ($package->getModels() as $model) {
					$engine->shop->addModel($model->getName(), $model);
			}

			/**
			 * Add components
			 */
			foreach ($package->getModules() as $adminModule) {
				// bdump($adminModule->getConfig());
				$components[$adminModule->getComponentName()]['modules'][] = $adminModule->getConfig();
			}
		}

		// $interfaces = get_declared_interfaces();
		// $classes = get_declared_classes();
		// $implemented = array();
		// $notImplemented = array();
		// foreach($classes as $class) {
		// 	$reflect = new \ReflectionClass($class);
		// 	if ($reflect->implementsInterface('\Redenge\Admin\IModulePackage')) {
		// 		$implemented[] = $class;
		// 	} else {
		// 		$notImplemented[] = $class;
		// 	}
		// }

		// bdump([
		// 	'total interfaces' => count($classes),
		// 	'implemented (\Redenge\Admin\IModulePackage)' => $implemented,
		// 	'notImplemented' => $notImplemented,
		// 	'interface list' => $interfaces,
		// ]);
	}

	/**
	 * @param array
	 */
	public function setCurrentModule(array $currentModule)
	{
		$this->currentModule = $currentModule;
	}

	/**
	 * @param string
	 */
	public function getCurrentModulePath($dir)
	{
		return isset($this->currentModule['extension']) ? $this->currentModule['extension'] : $dir;
	}

	/**
	 * @param \TemplatePower    $template
	 * @param array             $module
	 * @param string            $baseLink
	 * @param string            $componentName
	 */
	public function addModuleLinkToTemplate(&$template, array $module, $baseLink, $componentName)
	{
		$generateLink = isset($module['extension']) && $module['extension'] && \Nette\Utils\Strings::startsWith($module['extension'], ':');

		$template->assign('NAME', $module['caption']);
		$template->assign('LINK', $generateLink
			? $this->getLinkGenerator()->link(ltrim($module['extension'], ':'))
			: sprintf("%s?c=%s&amp;m=%s", $baseLink, $componentName, $module['name'])
		);

		$template->assign('ACTIVE', $generateLink
			? FALSE
			: \Nette\Utils\Strings::contains($_SERVER['REQUEST_URI'], sprintf('c=%s&m=%s', $componentName, $module['name'])) ? 'active' : ''
		);
	}

	/**
	 * @return \Nette\Application\LinkGenerator
	 */
	private function getLinkGenerator()
	{
		if ($this->linkGenerator === NULL) {
			/** @var \Nette\Application\LinkGenerator $linkGenerator */
			$this->linkGenerator = $this->engine->getService('application.linkGenerator');
		}

		return $this->linkGenerator;
	}
}
