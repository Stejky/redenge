<?php

namespace Redenge\Admin;

class Module implements IModule
{
	/** @var string */
	private $componentName;

	/** @var string */
	private $name;

	/** @var string */
	private $caption;

	/**
	 * @param string $componentName
	 * @param string $name
	 * @param string $caption
	 * @param string $modulePath
	 */
	public function __construct($componentName, $name, $caption, $modulePath)
	{
		$path = realpath($modulePath);
		if ($path === FALSE) {
			throw new \Exception(sprintf('Path for "%s" module "%s" does\'t exists (%s).', $componentName, $caption, $modulePath));
		}

		$this->componentName = $componentName;
		$this->name = $name;
		$this->caption = $caption;
		$this->path = $path;
	}

	/**
	 * @return string
	 */
	public function getComponentName()
	{
		return $this->componentName;
	}

	/**
	 * @return array
	 */
	public function getConfig()
	{
		return [
			'name' => $this->name,
			'caption' => $this->caption,
			'extension' => $this->path,
		];
	}
}
