<?php

namespace Redenge\Admin;

class Model implements IModel
{
	/** @var string */
	private $name;

	/** @var string */
	private $class;

	/**
	 * @param string $name
	 * @param string $className Fully-qualified class name
	 */
	public function __construct($name, $class)
	{
		$this->name = $name;
		$this->class = $class;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getClass()
	{
		return $this->class;
	}
}
