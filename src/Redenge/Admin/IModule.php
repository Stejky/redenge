<?php

namespace Redenge\Admin;

interface IModule
{
	/**
	 * @return string
	 */
	public function getComponentName();

	/**
	 * @return array
	 */
	public function getConfig();
}
