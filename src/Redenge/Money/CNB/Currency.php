<?php

namespace Redenge\Money\CNB;

use Nette\Utils\Strings;

class Currency implements ICurrency
{
	/** @var string */
	private $code;

	/** @var float */
	private $rate;

	/**
	 * @param string    $code
	 * @param float     $rate
	 */
	public function __construct($code, $rate)
	{
		$this->code = Strings::upper($code);
		$this->rate = $rate;
	}

	/**
	 * @return float
	 */
	public function getRate()
	{
		return $this->rate;
	}
}


interface ICurrency
{
	/**
	 * @return float
	 */
	function getRate();
}
