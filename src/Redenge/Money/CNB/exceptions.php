<?php

namespace Redenge\Money\CNB\Exception;

class Exception extends \Exception
{
}

class NotFoundException extends Exception
{
}

class NoCurrenciesException extends Exception
{
}
